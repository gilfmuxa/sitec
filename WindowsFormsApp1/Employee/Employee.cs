﻿using System.Collections.Generic;
using System.Linq;

namespace WindowsFormsApp1
{
    public enum Sort
    {
        name,
        rkk,
        request,
        sum
    }

    // Расширение, чтобы отображалось имя в нормальном виде
    public static class SortExtensions
    {
        public static string GetDescription(this Sort @this)
        {
            switch (@this)
            {
                case Sort.name:
                    return "По исполнителю";
                case Sort.rkk:
                    return "По РКК";
                case Sort.request:
                    return "По обращениям";
                case Sort.sum:
                    return "По общему количеству";
                default:
                    return "По исполнителю";
            }
        }
    }
    
    public class Employees : Dictionary<string, Document>
    {
        public void Add(string name, TypeOfDoc doc)
        {
            // В зависимости какой тип документа
            if (ContainsKey(name))
                if (doc == TypeOfDoc.Request)
                    this[name].CountDoc++;
                else
                    this[name].CountRKK++;
            else
                Add(name, new Document(doc));
        }

        // Сортировка в таком виде, т.к. легче потом через foreach работать
        public IOrderedEnumerable<KeyValuePair<string, Document>> Sorted(Sort sort)
        {
            switch (sort)
            {
                case Sort.name:
                        return this.OrderBy(o => o.Key);
                case Sort.rkk:
                        return this.OrderByDescending(o => o.Value.CountRKK).ThenByDescending(o => o.Value.CountDoc);
                case Sort.request:
                        return this.OrderByDescending(o => o.Value.CountDoc).ThenByDescending(o => o.Value.CountRKK);
                case Sort.sum:
                        return this.OrderByDescending(o => o.Value.Sum).ThenByDescending(o => o.Value.CountRKK);
                default:
                    return this.OrderBy(o => o.Key);
            }
        }
    }
}
