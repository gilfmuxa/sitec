﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public enum TypeOfDoc
    {
        RKK,
        Request
    }

    public class Document
    {
        private int countDoc;
        private int countRKK;
        private int sum;

        public int CountDoc 
        { 
            get
            {
                return countDoc;
            }
            
            set
            {
                countDoc++;
                sum = countDoc + countRKK;
            } 
        }

        public int CountRKK 
        {
            get
            {
                return countRKK;
            }

            set
            {
                countRKK++;
                sum = countDoc + countRKK;
            }
        }

        public int Sum 
        {
            get
            {
                return sum;
            }
        }

        public Document(TypeOfDoc doc)
        {
            if (doc == TypeOfDoc.Request)
                CountDoc++;
            else
                CountRKK++;
        }
    }
}
