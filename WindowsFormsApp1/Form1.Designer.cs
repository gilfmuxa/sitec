﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileRKK = new System.Windows.Forms.OpenFileDialog();
            this.openFileReq = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_WorkTime = new System.Windows.Forms.Label();
            this.btn_Download = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbl_Req = new System.Windows.Forms.Label();
            this.btn_File_Req = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbl_RKK = new System.Windows.Forms.Label();
            this.btn_File_RKK = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_Report = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_Sort = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dg_Report = new System.Windows.Forms.DataGridView();
            this.Num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_CountRKK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Doc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbl_DateTimeOrder = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_Report)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileRKK
            // 
            this.openFileRKK.Filter = "РКК|*РКК*.txt";
            // 
            // openFileReq
            // 
            this.openFileReq.Filter = "Обращения|*Обр*.txt";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbl_WorkTime);
            this.panel1.Controls.Add(this.btn_Download);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(321, 556);
            this.panel1.TabIndex = 0;
            // 
            // lbl_WorkTime
            // 
            this.lbl_WorkTime.AutoSize = true;
            this.lbl_WorkTime.Location = new System.Drawing.Point(90, 128);
            this.lbl_WorkTime.Name = "lbl_WorkTime";
            this.lbl_WorkTime.Size = new System.Drawing.Size(108, 13);
            this.lbl_WorkTime.TabIndex = 3;
            this.lbl_WorkTime.Text = "Время выполнения:";
            // 
            // btn_Download
            // 
            this.btn_Download.Location = new System.Drawing.Point(9, 123);
            this.btn_Download.Name = "btn_Download";
            this.btn_Download.Size = new System.Drawing.Size(75, 23);
            this.btn_Download.TabIndex = 7;
            this.btn_Download.Text = "Загрузить";
            this.btn_Download.UseVisualStyleBackColor = true;
            this.btn_Download.Click += new System.EventHandler(this.btn_Download_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbl_Req);
            this.groupBox2.Controls.Add(this.btn_File_Req);
            this.groupBox2.Location = new System.Drawing.Point(3, 63);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(309, 54);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Выберите обращения";
            // 
            // lbl_Req
            // 
            this.lbl_Req.AutoSize = true;
            this.lbl_Req.Location = new System.Drawing.Point(87, 24);
            this.lbl_Req.Name = "lbl_Req";
            this.lbl_Req.Size = new System.Drawing.Size(92, 13);
            this.lbl_Req.TabIndex = 5;
            this.lbl_Req.Text = "Файл не выбран";
            // 
            // btn_File_Req
            // 
            this.btn_File_Req.Location = new System.Drawing.Point(6, 19);
            this.btn_File_Req.Name = "btn_File_Req";
            this.btn_File_Req.Size = new System.Drawing.Size(75, 23);
            this.btn_File_Req.TabIndex = 4;
            this.btn_File_Req.Text = "Обзор...";
            this.btn_File_Req.UseVisualStyleBackColor = true;
            this.btn_File_Req.Click += new System.EventHandler(this.btn_File_Req_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbl_RKK);
            this.groupBox1.Controls.Add(this.btn_File_RKK);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(309, 54);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Выберите РКК";
            // 
            // lbl_RKK
            // 
            this.lbl_RKK.AutoSize = true;
            this.lbl_RKK.Location = new System.Drawing.Point(87, 24);
            this.lbl_RKK.Name = "lbl_RKK";
            this.lbl_RKK.Size = new System.Drawing.Size(92, 13);
            this.lbl_RKK.TabIndex = 2;
            this.lbl_RKK.Text = "Файл не выбран";
            // 
            // btn_File_RKK
            // 
            this.btn_File_RKK.Location = new System.Drawing.Point(6, 19);
            this.btn_File_RKK.Name = "btn_File_RKK";
            this.btn_File_RKK.Size = new System.Drawing.Size(75, 23);
            this.btn_File_RKK.TabIndex = 1;
            this.btn_File_RKK.Text = "Обзор...";
            this.btn_File_RKK.UseVisualStyleBackColor = true;
            this.btn_File_RKK.Click += new System.EventHandler(this.btn_File_RKK_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lbl_DateTimeOrder);
            this.panel3.Controls.Add(this.btn_Report);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.comboBox_Sort);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(321, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(664, 67);
            this.panel3.TabIndex = 0;
            // 
            // btn_Report
            // 
            this.btn_Report.Location = new System.Drawing.Point(198, 15);
            this.btn_Report.Name = "btn_Report";
            this.btn_Report.Size = new System.Drawing.Size(75, 36);
            this.btn_Report.TabIndex = 2;
            this.btn_Report.Text = "Выгрузить отчет";
            this.btn_Report.UseVisualStyleBackColor = true;
            this.btn_Report.Click += new System.EventHandler(this.Btn_Report_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Сортировка";
            // 
            // comboBox_Sort
            // 
            this.comboBox_Sort.FormattingEnabled = true;
            this.comboBox_Sort.Items.AddRange(new object[] {
            "По исполнителю",
            "По РКК",
            "По обращениям",
            "По общему количеству"});
            this.comboBox_Sort.Location = new System.Drawing.Point(6, 25);
            this.comboBox_Sort.Name = "comboBox_Sort";
            this.comboBox_Sort.Size = new System.Drawing.Size(186, 21);
            this.comboBox_Sort.TabIndex = 0;
            this.comboBox_Sort.SelectedIndexChanged += new System.EventHandler(this.ComboBox1_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dg_Report);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(321, 67);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(664, 489);
            this.panel2.TabIndex = 1;
            // 
            // dg_Report
            // 
            this.dg_Report.AllowUserToAddRows = false;
            this.dg_Report.AllowUserToDeleteRows = false;
            this.dg_Report.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_Report.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_Report.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Num,
            this.Col_Name,
            this.Col_CountRKK,
            this.Doc,
            this.Sum});
            this.dg_Report.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_Report.Location = new System.Drawing.Point(0, 0);
            this.dg_Report.Name = "dg_Report";
            this.dg_Report.ReadOnly = true;
            this.dg_Report.RowHeadersVisible = false;
            this.dg_Report.Size = new System.Drawing.Size(664, 489);
            this.dg_Report.TabIndex = 0;
            // 
            // Num
            // 
            this.Num.FillWeight = 38.07106F;
            this.Num.HeaderText = "№";
            this.Num.Name = "Num";
            this.Num.ReadOnly = true;
            // 
            // Col_Name
            // 
            this.Col_Name.FillWeight = 115.4822F;
            this.Col_Name.HeaderText = "Ответственный исполнитель";
            this.Col_Name.Name = "Col_Name";
            this.Col_Name.ReadOnly = true;
            // 
            // Col_CountRKK
            // 
            this.Col_CountRKK.FillWeight = 115.4822F;
            this.Col_CountRKK.HeaderText = "Количество неисполненных входящих документов";
            this.Col_CountRKK.Name = "Col_CountRKK";
            this.Col_CountRKK.ReadOnly = true;
            // 
            // Doc
            // 
            this.Doc.FillWeight = 115.4822F;
            this.Doc.HeaderText = "Количество неисполненных письменных обращений граждан";
            this.Doc.Name = "Doc";
            this.Doc.ReadOnly = true;
            // 
            // Sum
            // 
            this.Sum.FillWeight = 115.4822F;
            this.Sum.HeaderText = "Общее количество документов и обращений";
            this.Sum.Name = "Sum";
            this.Sum.ReadOnly = true;
            // 
            // lbl_DateTimeOrder
            // 
            this.lbl_DateTimeOrder.AutoSize = true;
            this.lbl_DateTimeOrder.Location = new System.Drawing.Point(279, 15);
            this.lbl_DateTimeOrder.Name = "lbl_DateTimeOrder";
            this.lbl_DateTimeOrder.Size = new System.Drawing.Size(140, 13);
            this.lbl_DateTimeOrder.TabIndex = 3;
            this.lbl_DateTimeOrder.Text = "Дата составления отчета:\r\n";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(985, 556);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Отчет";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_Report)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileRKK;
        private System.Windows.Forms.OpenFileDialog openFileReq;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_Download;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lbl_Req;
        private System.Windows.Forms.Button btn_File_Req;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbl_RKK;
        private System.Windows.Forms.Button btn_File_RKK;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dg_Report;
        private System.Windows.Forms.DataGridViewTextBoxColumn Num;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_CountRKK;
        private System.Windows.Forms.DataGridViewTextBoxColumn Doc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_Sort;
        private System.Windows.Forms.Button btn_Report;
        private System.Windows.Forms.Label lbl_WorkTime;
        private System.Windows.Forms.Label lbl_DateTimeOrder;
    }
}

