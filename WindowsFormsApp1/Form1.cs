﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        // Для работы с файлами
        readonly Files files = new Files();

        // Сотрудников сохраняем в словарь
        Employees employees = new Employees();

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_File_RKK_Click(object sender, EventArgs e)
        {
            openFileRKK.ShowDialog();
            files.PathRKK = openFileRKK.FileName;

            lbl_RKK.Text = files.GetNameRKK();
        }

        private void btn_File_Req_Click(object sender, EventArgs e)
        {
            openFileReq.ShowDialog();
            files.PathReq = openFileReq.FileName;

            lbl_Req.Text = files.GetNameReq();
        }

        private void btn_Download_Click(object sender, EventArgs e)
        {
            employees = new Employees();

            // Передаем ссылкой, чтоб на выходе уже можно было работать. 
            files.LoadEmployees(ref employees);

            lbl_WorkTime.Text = "Время выполнения: " +
                files.WorkTime().Seconds + "," +
                files.WorkTime().Milliseconds + " сек";

            LoadDataGrid(Sort.name);
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectSorted = (Sort)comboBox_Sort.SelectedIndex;
            
            LoadDataGrid(selectSorted);
        }

        private void LoadDataGrid(Sort sort)
        {
            dg_Report.Rows.Clear();

            var i = 1;

            var sorted = employees.Sorted(sort);
            
            foreach (var item in sorted)
            {
                dg_Report.Rows.Add(new string[]
                {
                    i.ToString(),
                    item.Key,
                    item.Value.CountRKK.ToString(),
                    item.Value.CountDoc.ToString(),
                    item.Value.Sum.ToString()
                });

                i++;
            }
        }

        private void Btn_Report_Click(object sender, EventArgs e)
        {
            Report report = new Report(employees);

            Sort sort = (Sort)comboBox_Sort.SelectedIndex;

            report.CreateRTF(sort);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lbl_WorkTime.Text = "";

            lbl_DateTimeOrder.Text += DateTime.Now.ToShortDateString();

            comboBox_Sort.SelectedIndex = 0;
        }
    }
}
