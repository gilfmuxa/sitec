﻿using System;
using System.IO;

namespace WindowsFormsApp1
{
    public class Files
    {
        private TimeSpan workTime;
        private const string klimov = "Климов Сергей Александрович";

        public string PathReq { get; set; }
        public string PathRKK { get; set; }

        public string GetNameRKK()
        {
            var index = PathRKK.LastIndexOf('\\');
            var name = PathRKK.Remove(0, index + 1);

            return name;
        }

        public string GetNameReq()
        {
            var index = PathReq.LastIndexOf('\\');
            var name = PathReq.Remove(0, index + 1);

            return name;
        }

        public void LoadEmployees(ref Employees array)
        {
            if (string.IsNullOrEmpty(PathReq) ||
                string.IsNullOrEmpty(PathRKK))
            {
                System.Windows.Forms.MessageBox.Show("Выберите все файлы");
                return;
            }

            if (!File.Exists(PathReq) ||
                !File.Exists(PathRKK))
            {
                System.Windows.Forms.MessageBox.Show("Файлов не существует");
                return;
            }

            DateTime dateTime = DateTime.Now;

            var timeBefore = dateTime;

            Load(ref array, PathReq, TypeOfDoc.Request);
            Load(ref array, PathRKK, TypeOfDoc.RKK);

            dateTime = DateTime.Now;

            var timeAfter = dateTime;

            workTime = (timeAfter - timeBefore);
        }

        /// <summary>
        /// Редактируем сотрудников и добавляем в массив
        /// </summary>
        /// <param name="array"></param>
        /// <param name="path">Путь</param>
        /// <param name="doc">Тип документа</param>
        private void Load(ref Employees array, string path, TypeOfDoc doc)
        {
            using (var sr = new StreamReader(path, System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    // До табуляции разибавем строку и проверяем равняется ли Климову
                    var employee = line.Split('\t').GetValue(0).ToString();

                    // Если Климов, то вытаскиваем иполнителя
                    if (employee.Equals(klimov))
                    {
                        employee = line.Split('\t').GetValue(1).ToString().Split(';').GetValue(0).ToString();

                        employee = ToOnlyShortName(employee);
                    }
                    else
                    {
                        // Иначе будет руководитель
                        employee = ToShortName(employee);
                    }

                    array.Add(employee, doc);
                }
            }
        }

        // Для руководителей
        private string ToShortName (string str)
        {
            var index = str.IndexOf(' ');

            var firstName = str.Remove(index);
            var secondName = str.Split(' ').GetValue(1).ToString()[0];
            var thirdName = str.Split(' ').GetValue(2).ToString()[0];

            return $"{firstName} {secondName}.{thirdName}.";
        }

        // Для исполнителей
        private string ToOnlyShortName(string str)
        {
            var index = str.IndexOf('(');

            // Удаляем также лишний пробел
            if (index > 0)
                str = str.Remove(index - 1);

            return str;
        }

        public TimeSpan WorkTime() => workTime;
    }
}
