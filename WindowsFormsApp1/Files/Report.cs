﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    class Report
    {
        Employees employees;
        public Report(Employees _employees)
        {
            employees = _employees;
        }

        public void CreateRTF(Sort sort)
        {
            RichTextBox richTextBox = new RichTextBox();

            richTextBox.SelectionFont = new Font("Times New Roman", 14);
            
            StringBuilder tableRtf = new StringBuilder();
            tableRtf.Append(@"{\rtf1\ansi\deff0 {\fonttbl {\f0 Times New Roman;}}");
            tableRtf.Append(@"{\pard \qc \fs28 \b Справка о неисполненных документах и обращениях граждан \b0\par}");
            tableRtf.Append(@"{\pard \sb120 \fs24 Не исполнено в срок " + employees.Sum(s => s.Value.Sum) + @" документов, из них:\par}");
            tableRtf.Append(@"{\pard \sb40 \li240 - количество неисполненных входящих документов: " + employees.Sum(s => s.Value.CountRKK) + @"\par}");
            tableRtf.Append(@"{\pard \sb40 \li240 - количество неисполненных письменных обращений граждан: " + employees.Sum(s => s.Value.CountDoc) + @"\par}");
            tableRtf.Append(@"{\pard \sb40 \par}");

            tableRtf.Append(@"{\pard Сортировка: \b " + sort.GetDescription() + @". \b0 \par}");
            tableRtf.Append(@"{\pard \sb40 \par}");

            tableRtf.Append(@"{\fonttbl{\f0\fnil\fcharset0 Times New Roman CYR;}}");
            tableRtf.Append(@"\trowd\trleft0\trgaph120");
            tableRtf.Append(@"\cellx700");
            tableRtf.Append(@"\cellx2900");
            tableRtf.Append(@"\cellx4900");
            tableRtf.Append(@"\cellx6900");
            tableRtf.Append(@"\cellx8900");
            tableRtf.Append($@"\intbl № п.п.\cell");
            tableRtf.Append($@"\intbl Ответственный исполнитель\cell");
            tableRtf.Append($@"\intbl Количество неисполненных входящих документов\cell");
            tableRtf.Append($@"\intbl Количество неисполненных письменных обращений граждан\cell");
            tableRtf.Append($@"\intbl Общее количество документов и обращений\cell");
            tableRtf.Append(@"\intbl\clmrg\cell\row");

            var i = 1;
            var sorted = employees.Sorted(sort);

            foreach (var item in sorted)
            {
                tableRtf.Append(@"\trowd\trleft0\trgaph120");
                tableRtf.Append(@"\cellx700");
                tableRtf.Append(@"\cellx2900");
                tableRtf.Append(@"\cellx4900");
                tableRtf.Append(@"\cellx6900");
                tableRtf.Append(@"\cellx8900");
                tableRtf.Append($@"\intbl {i}\cell");
                tableRtf.Append($@"\intbl {item.Key}\cell");
                tableRtf.Append($@"\intbl {item.Value.CountRKK}\cell");
                tableRtf.Append($@"\intbl {item.Value.CountDoc}\cell");
                tableRtf.Append($@"\intbl {item.Value.Sum}\cell");
                tableRtf.Append(@"\intbl\clmrg\cell\row");

                i++;
            }

            tableRtf.Append(@"\pard");
            tableRtf.Append(@"}");

            string rtf1 = richTextBox.Rtf.Trim().TrimEnd('}');
            string rtf2 = tableRtf.ToString();
            richTextBox.Rtf = rtf1 + rtf2;

            richTextBox.SelectionFont = new Font("Times New Roman", 12);
            richTextBox.SelectionAlignment = HorizontalAlignment.Left;
            richTextBox.AppendText("\n\nДата составления справки:\t" + DateTime.Now.ToShortDateString());
            richTextBox.AppendText("\n\n");

            var path = Path.Combine(Directory.GetCurrentDirectory(), "Report.rtf");
            richTextBox.SaveFile(path, RichTextBoxStreamType.RichText);

            Process.Start(path);
        }
    }
}
